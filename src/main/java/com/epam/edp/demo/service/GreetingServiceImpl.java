package com.epam.edp.demo.service;

import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {

    private final ServiceRepository serviceRepository;

    @Override
    public String getRandomGreeting() {
        return serviceRepository.findGreeting();
    }
}
